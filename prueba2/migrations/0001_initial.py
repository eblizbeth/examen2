# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='User',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('password', models.CharField(verbose_name='password', max_length=128)),
                ('last_login', models.DateTimeField(verbose_name='last login', null=True, blank=True)),
                ('tipo_usuario', models.CharField(verbose_name='Tipo de usuario', max_length=30)),
                ('creditos', models.IntegerField(verbose_name='Créditos', null=True, blank=True)),
                ('nombre', models.CharField(verbose_name='Nombre', max_length=100)),
                ('email', models.EmailField(max_length=70, unique=True, blank=True)),
                ('nombre_usuario', models.CharField(verbose_name='Nombre de usuario', max_length=100)),
                ('joined', models.DateTimeField(auto_now_add=True)),
                ('is_active', models.BooleanField(default=True)),
                ('is_admin', models.BooleanField(default=False)),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
