# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('prueba2', '0002_auto_20161213_2159'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='User',
            new_name='Usuario',
        ),
    ]
