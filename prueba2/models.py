from django.db import models
#from django.contrib.auth.models import AbstractBaseUser

class Usuario(models.Model):
    tipo_usuario = models.CharField(max_length=30,verbose_name='Tipo de usuario')
    creditos = models.IntegerField(blank=True,null=True, verbose_name='Créditos')
    nombre = models.CharField(max_length=100,verbose_name='Nombre')
    email = models.EmailField(max_length=70,blank=True,unique=True)
    nombre_usuario = models.CharField(max_length=100,verbose_name='Nombre de usuario')
    #joined = models.DateTimeField(auto_now_add=True)
    #is_active = models.BooleanField(default=True)
    #is_admin = models.BooleanField(default=False)
    def __str__(self):
        return '{}'.format(self.nombre)

class TipoUsuario(models.Model):
    tipo_usuario = models.CharField(max_length=30,verbose_name='Tipo de usuario')

    def __str__(self):
        return '{}'.format(self.tipo_usuario)